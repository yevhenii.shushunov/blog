const express = require('express');
const router = express.Router();
const userController = require('../controller/user.controller'),
    blogController = require ('../controller/blog.controller'),
    authenticate = require('../middleware/authenticate'),
    body = require('../middleware/inputValidator');

router.post('/login', body.emailValidator, body.passwordValidator, userController.login);
router.get('/logout', userController.logout);



router.get('/blog/all', 
    authenticate,
    blogController.getAllPosts);
router.post('/blog/add',
    body.isEpty,
    authenticate, 
    blogController.addPost);
router.put('/blog/:id',
    body.isId,
    body.isEpty,
    authenticate,
    blogController.updatePost);
router.delete('/blog/:id',
    body.isId,
    authenticate,
    blogController.deletePost);

module.exports = router;
