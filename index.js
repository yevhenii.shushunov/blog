const express = require ('express'),
        config = require('./config'),
        swaggerJsDoc = require('swagger-jsdoc'),
        swaggerUi = require('swagger-ui-express'),
        options = require('./models/option'),
        session = require('express-session'),
        router = require('./routes/routes.module');
       
const app = express(),
  sessionTime = 3600*60,
  specs = swaggerJsDoc(options);

app.use(express.json({limit: "1kb"}));
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs),
);

app.use(
  session({
    secret: config.SecretKey,
    resave: false,
    saveUninitialized: true,
    cookie: {
      secure: false,
      httpOnly: false,
      maxAge: sessionTime}
  })
)

app.use(router);

app.use(
  express.urlencoded({
    extended: true,
    limit: "1kb",
  })
)


app.listen(config.PORT, ()=> {
    console.log(`server working on port: ${config.PORT}`);
});