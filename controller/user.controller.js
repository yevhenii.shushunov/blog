const userServise = require('../services/user.services');

module.exports = {
    login,
    logout
};

async function login(req, res) {
    const {login, password} = req.body;
    try {
        const user = await userServise.authorization(login, password);
            req.session.user = user;
            res.status(200).send('succes signin');
    } catch(e) {
        res.status(500).send(`signin failure ${e}`);
    } 
}  

function logout(req, res) {
    req.session.destroy();
    res.status(200).send('logout');
}


