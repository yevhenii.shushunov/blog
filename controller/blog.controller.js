const blogServise = require('../services/blog.services')

module.exports = {
    getAllPosts,
    addPost,
    updatePost,
    deletePost
};


    async function getAllPosts(req, res) {
        try{
            const posts = await blogServise.getAllBlogPosts();
            res.status(200).json(posts);
        }catch(e){
            res.status(400).send(e);
        }
        
    }

    async function addPost(req, res) {
        const {title, post, author} = req.body;
            try{
                const response = await blogServise.createPost(title, post, author);
                res.status(200).send(response);
            }catch(e){
                res.status(500).send(`error: ${e}`);
            } 
    }

    async function updatePost(req, res) {
        const id = req.params.id;
        const {title, post, author} = req.body;
            try{
                const selectedPost = await blogServise.updatePostById(id, title, post, author);
                res.status(200).send(selectedPost); 
            }catch(e){
                res.status(400).send(`error: ${e}`)
            }
    }

    async function deletePost(req, res) {
        const id = req.params.id; 
            try{
                const response = await blogServise.deletePostById(id);
                res.status(200).send(response)
            }catch(e){
                res.status(400).send(`error: ${e}`);
            } 
    }


