const Pool = require('pg').Pool,
    config = require('../config');

const pool = new Pool({
    user: config.DBUser,
    password: config.DBPassword,
    host: "localhost",
    port: 5432,
    database: "blog",
})

module.exports = pool;
