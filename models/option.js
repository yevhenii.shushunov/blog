const options = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "LogRocket Express API with Swagger",
        version: "0.1.0",
        description:
          "Test API with Express and documented with Swagger",
        license: {
          name: "MIT",
          url: "https://spdx.org/licenses/MIT.html",
        },
        contact: {
          name: "Yevhenii",
          url: "#",
          email: "yevhenii.shushunov@codeit.pro",
        },
      },
      servers: [
        {
          url: "http://localhost:3001",
        },
      ],
    },
    apis: ["./models/blog.js"],
  };

  module.exports = options;