/**
 * @openapi
 * securityDefinitions:
 *  basicAuth:
 *      type: basic
 * components:
 *  securitySchemes:
 *      Swagger_auth:
 *          type: http
 *          scheme: basic
 * security:
 *  - basicAuth: [user]
 * 
 * paths:
 *  /login:
 *   post:
 *       tags:
 *          - authorization
 *       summary: login
 *       requestBody:
 *           required: true
 *           content: 
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/user'
 *       responses:
 *           '204':
 *               description: succes
 *           '401':
 *               description: bad authorization
 *  /logout:
 *   get:
 *     tags:
 *      - authorization
 *     summary: logout
 *     description: logout
 *     responses:
 *       '200':
 *         description: A successful response
 * 
 *  /blog/all:
 *   get:
 *       tags:
 *           - posts
 *       summary: get all posts
 *       description: Use to request all blogs
 *       responses:
 *           '200':
 *               description: A successful response
 *       security:
 *          - basicAuth: [] 
 *  /blog/add:
 *   post:
 *       tags:
 *         - posts
 *       summary: create post in blog
 *       consumed:
 *          - application/x-www-form-urlencoded
 *       produced:
 *           application/json
 *       requestBody:
 *           required: true 
 *           content:
 *             application/json:
 *               schema:
 *                   $ref: '#/components/schemas/blog'           
 *       responses:
 *           '200':
 *               description: The post was created
 *           '500':
 *               description: server error
 *       security:
 *           - basicAuth: []
 *  /blog/{id}:
 *   delete:
 *    tags:
 *      - posts
 *    summary: delete the post
 *    operationId: deleteUser
 *    parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       schema:
 *           type: integer
 *    responses:
 *        '200':
 *            description: post deleted
 *        '400':
 *            description: invalid post id
 *    security:
 *        - basicAuth: [] 
 *   put:
 *       tags:
 *         - posts
 *       summary: update the post
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *               type: integer
 *           required: true
 *       requestBody:
 *           required: true
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/blog' 
 *       responses:
 *           '200':
 *               description: post updated
 *           '400':
 *               description: post not find
 *       security:
 *           - basicAuth: [] 
 */
 /**
 * @openapi
 *  components:
 *   schemas:
 *      blog:
 *          type: object
 *          properties:
 *              title:
 *                  type: string
 *                  description: title of your post
 *              post:
 *                  type: string
 *                  description: your post text
 *              author:
 *                  type: string
 *                  description: post author
 *      user:
 *          type: object
 *          properties:
 *              login:
 *                  type: string
 *              password:
 *                  type: string
 */
