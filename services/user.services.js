const db = require('../models/db');

module.exports = {
    authorization,
}

async function authorization (login, password){
    try{
        const response = await db.query('SELECT login, password FROM users WHERE login = $1 AND password = $2;', [login, password]);
        if(response.rows[0].login){
            return response.rows[0].login;
        } else {
            return new Error('user not found')
        }
    }catch(e){
        return e
    }
    
    
}


