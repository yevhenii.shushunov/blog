const db = require('../models/db');

module.exports = {
    getAllBlogPosts,
    createPost,
    updatePostById,
    deletePostById,
}

async function getAllBlogPosts(){
    const posts = await db.query(`SELECT id, title, post, author, date FROM post;`);
    return posts;     
}

async function createPost (title, post, author) {
    const response = await db.query(`INSERT INTO post (title, post, author, date) VALUES ($1, $2, $3, NOW()) RETURNING *`, [title, post, author])
    if(response.rows.length === 0) throw new Error('post not created');
    return (response.rows[0]);      
}

async function updatePostById(id, title, post, author ){
    const update = await db.query(`UPDATE post SET title = $1, post = $2, author = $3, date = NOW() WHERE id = $4 RETURNING *;`, [title, post, author, id]);
    if (update.rows.length === 0) throw new Error('post not found')
    return (update.rows[0]);    
}

async function deletePostById(id){
    const post = await db.query(`DELETE FROM post WHERE id = $1`, [id]);
    if(post.rowCount === 0) throw new Error(`post ${id} not found`)
    return `post ${id} deleted`;             
}