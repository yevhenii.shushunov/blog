function authenticate(req, res, next) {
    if(!req.session || !req.session.user){
        const err = new Error('unauthorized user');
        res.status(401);
        //err.statusCode = 401;
        next(err)
        return
    }
    next();
}

module.exports = authenticate;