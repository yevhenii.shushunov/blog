module.exports = {
    passwordValidator,
    emailValidator,
    isEpty,
    isId,
};


function passwordValidator (req, res, next) {
    const body = req.body.password;

    if(body.length > 60 || Object.keys(body).length === 0 || body.length > 20 || body.length < 6){
        const err = new Error('wrong password');
        err.statusCode = 401;
        next(err);
    }
    next();
}

function emailValidator(req, res, next) {
    const login = req.body.login;
    const isEmail = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(!login.match(isEmail)){
        const err = new Error('login is not valid');
        err.statusCode = 401;
        next(err);
    }
    next();
}

function isEpty (req, res, next) {
    const body = req.body;
    if(Object.keys(body).length === 0) {
        const err = new Error('data is empty');
        next(err);
    }
    next();
}

function isId(req, res, next){
    const body = req.params.id;
    if(body.length === 0){
        const err = new Error('data is empty');
        next(err);
    }
    next()
}


